class Resistencia{
   float xp, yp;
   
   float ancho, alto;
   Rectangulo barras[];
   
   Resistencia(float a, float b, float c, float d){
       xp = a;
       yp = b;
       ancho = c;
       alto = d;
       barras = new Rectangulo[4];
       barras[0] = new Rectangulo(xp+20,yp,20,alto,0);
       barras[1] = new Rectangulo(xp+2*20,yp,20,alto,1);
       barras[2] = new Rectangulo(xp+3*20,yp,20,alto,2);
       barras[3] = new Rectangulo(xp+4*20,yp,20,alto,3);
   }
   
   public void dibujarResistencia(){
       
       for(int i=0;i < barras.length;i++){
           fill(barras[i].pintado);
           rect(barras[i].xp+(5*i), barras[i].yp, barras[i].ancho, barras[i].alto);
       }
       noFill();
       strokeWeight(3);
       rect(xp,yp,ancho,alto);
       line(xp-50,yp+(alto/2), xp,yp+(alto/2) );
       line(xp+ancho,yp+(alto/2),xp+ancho+50,yp+(alto/2));
       strokeWeight(1);
   }


}